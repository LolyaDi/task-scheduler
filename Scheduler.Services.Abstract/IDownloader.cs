﻿using System.Threading.Tasks;

namespace Scheduler.Services.Abstract
{
    public interface IDownloader
    {
        Task<string> Download(string url, string filePath);
    }
}
