﻿using System;

namespace Scheduler.Models
{
    public class Event
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Name { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public PeriodicityType Periodicity { get; set; }
    }
}