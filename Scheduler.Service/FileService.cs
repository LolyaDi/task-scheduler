﻿using Scheduler.Services.Abstract;
using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scheduler.Service
{
    public class FileService : IDownloader
    {
        private bool CheckFilePath(string filePath)
        {
            if (filePath != "" && Regex.IsMatch(filePath, @"^(?:[\a-zA-Z]\:|\\)(\\[a-zA-Z_\-\s0-9\.]+)+\.([A-Za-z0-9]+)$"))
            {
                return true;
            }
            return false;
        }

        private bool HasCreatedDirectory(string filePath)
        {
            string path = filePath.Substring(0, filePath.LastIndexOf(@"\"));

            try
            {
                var directory = new DirectoryInfo(path);
                if (!directory.Exists)
                {
                    directory.Create();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<string> Download(string url, string filePath)
        {
            if (!CheckFilePath(filePath))
            {
                return "Check your file path!";
            }
            else if (File.Exists(filePath))
            {
                return "File already exists!";
            }
            else if (!HasCreatedDirectory(filePath))
            {
                return "Try to choose another path for saving!";
            }

            try
            {
               await Task.Run(() => new WebClient().DownloadFile(new Uri(url), filePath));
            }
            catch (WebException)
            {
                return "File couldn't be downloaded!";
            }
            catch
            {
                return "Unpredictable error!";
            }

            return "File was successfully downloaded!";
        }


        public string Move(string fromDirectory, string toDirectory)
        {
            if (!CheckFilePath(fromDirectory))
            {
                return "Check your 'from:' file path!";
            }
            else if (!File.Exists(fromDirectory))
            {
                return "File doesn't exist!!!";
            }

            if (!CheckFilePath(toDirectory))
            {
                return "Check your 'to:' file path!";
            }
            else if (File.Exists(toDirectory))
            {
                return "File already exists!";
            }
            else if (!HasCreatedDirectory(toDirectory))
            {
                return "Try to choose another path for saving!";
            }

            try
            {
                File.Move(fromDirectory, toDirectory);
            }
            catch
            {
                return "Unpredictable error!";
            }

            return "File was successfully moved to another directory!";
        }
    }
}
