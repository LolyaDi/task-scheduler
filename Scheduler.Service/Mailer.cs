﻿using Scheduler.Services.Abstract;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Scheduler.Service
{
    public class Mailer : IMailer
    {
        public async Task<string> SendMail(string toMail, string fromMail, string fromMailPassword, string mailSubject, string mailText)
        {
            var host = "smtp.gmail.com";
            var port = 587;

            var client = new SmtpClient
            {
                Port = port,
                Host = host,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(fromMail, fromMailPassword),
                EnableSsl = true
            };

            var mail = new MailMessage
            {
                Subject = mailSubject,
                Body = mailText
            };

            try
            {
                mail.From = new MailAddress(fromMail);
                mail.To.Add(new MailAddress(toMail));
            }
            catch (ArgumentNullException)
            {
                return "Please add the recipient mail address!";
            }
            catch (FormatException)
            {
                return "Please check mail addresses!";
            }

            try
            {
                await client.SendMailAsync(mail);
            }
            catch (SmtpFailedRecipientsException)
            {
                return "Mail couldn't be sent to every recipient!";
            }
            catch (SmtpException)
            {
                return "Mail couldn't be sent!";
            }
            catch
            {
                return "Unpredictable error!";
            }

            return "Mail was successfully sent!";
        }
    }
}
