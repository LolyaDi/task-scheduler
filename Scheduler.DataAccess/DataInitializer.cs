﻿using Scheduler.Service;
using System.Data.Entity;

namespace Scheduler.DataAccess
{
    public class DataInitializer: DropCreateDatabaseAlways<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            context.Users.Add(new Models.User
            {
                Email = "beisenova.di5@gmail.com",
                Password = SecurityHasher.HashPassword("123_Lbkmyfp")
            });
        }
    }
}
